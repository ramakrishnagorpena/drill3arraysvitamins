const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/ 

// 1. Get all items that are available

const availableItems = items.filter((availables) => {
    return availables.available == true;
});

console.log(availableItems);


// 2. Get all items containing only Vitamin C.

const vitaminC = items.find((vitamin) => {
    return vitamin.contains = 'Vitamin C'
})

console.log(vitaminC);


// 3. Get all items containing Vitamin A.

const vitaminA = items.filter((vitamin) => {

    return vitamin.contains.includes('Vitamin A')
})

console.log(vitaminA);


// 4. Group items based on the Vitamins that they contain in the following format:
//         {
//             "Vitamin C": ["Orange", "Mango"],
//             "Vitamin K": ["Mango"],
//         }
        
//         and so on for all items and all Vitamins.

const GroupItems = items.reduce((vitamins, item)=>{
    const array =item.contains.split(', ');
    array.map((element)=>{
        if(vitamins.hasOwnProperty(element)){
            vitamins[element].push(item.name);
        } else{
            vitamins[element]= [];
            vitamins[element].push(item.name);
        }
    })
    return vitamins;
}, {})

console.log(GroupItems);


// 5. Sort items based on number of Vitamins they contain.

const sortNofVitamins = items.sort((a, b)=>{
    const aValue =a.contains.split(',').length;
    const bValue =b.contains.split(',').length;
    if(aValue>bValue){
        return 1;
    }
    if(bValue>aValue){
        return -1;
    }
    else{
          return 0;
    }
    
})
console.log(sortNofVitamins)